/**
 * Добавление 0 к числу
 *
 * @param {String} num
 * @returns {String}
 */
export const padTo2Digits = (num) => {
  return num.toString().padStart(2, '0');
};

/**
 * Форматирование даты из yyyy-MM-dd в dd.MM.yyyy
 *
 * @param {String} date
 * @returns {String}
 */
export const formatDate = (date) => {
  const dateObject = new Date(date);
  return `${padTo2Digits(dateObject.getDate())}.${padTo2Digits(dateObject.getMonth() + 1)}.${dateObject.getFullYear()}`;
};

/**
 * Разделение числа на тысячи
 *
 * @param {Number} num
 * @returns {String}
 */
export const formatMoney = (num) => {
  return String(num).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ').replace(',', '.');
};

/**
 * Возращает строку с первой заглавной буквой
 *
 * @param {String} string
 * @returns {String}
 */
export const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

import axios from 'axios';
import mocks from '@/mocks/getPayments';

/**
 * @var {Axios}
 */
const instance = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
});

/**
 * Load payments data.
 *
 * @param {Object} params
 * @returns {Promise}
 */
const getPayments = (params = {}) => {
  let res;

  if (IS_DEVELOPMENT) {
    res = mocks();
  } else {
    res = instance.request({
      method: 'get',
      url: '/api/v1/payments',
      params,
    });
  }

  return res;
};

export default {
  instance,
  getPayments,
};

/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const webpack = require('webpack');

const { NODE_ENV } = process.env;
const env = process.env.NODE_ENV;

module.exports = {
  devServer: {
    host: 'localhost',
    port: '8088',
    contentBase: path.resolve(__dirname, 'dist'),
    watchOptions: {
      poll: true,
    },
    historyApiFallback: true,
    overlay: false,
  },
  configureWebpack: {
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 6,
      }),
      new webpack.DefinePlugin({
        IS_DEVELOPMENT: env === 'development',
      }),
      new webpack.ProvidePlugin({
        Vue: ['vue', 'default'],
        mapState: ['vuex', 'mapState'],
        mapGetters: ['vuex', 'mapGetters'],
        mapMutations: ['vuex', 'mapMutations'],
        mapActions: ['vuex', 'mapActions'],
      }),
    ],
  },
  css: {
    sourceMap: NODE_ENV !== 'production',
  },
};
